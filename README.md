# Wine-TIM
TIM WINE 安装

## 安装
```bash
# 方法一（git 远程安装）
wineapp -i https://jihulab.com/wineapp/wine-tim.git

# 方法二 (中心库 远程安装)
wineapp -i com.tim.im.wine

# 方法三 本地安装
git clone https://jihulab.com/wineapp/wine-tim.git
cd wine-tim
wineapp -i
```

## 中心库
- https://jihulab.com/wineapp/winecenter.git
```bash
# 搜索
wineapp search com.qq.tim.wine
```
